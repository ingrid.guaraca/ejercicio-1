# Programa para convertir la temperatura en grados Celsius a grados Fahrenheit
#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca.unl.edu.ec"
centígrados = float (input("Ingrese la temperatura de grados centígrados: \n"))
fahrenheit = (centígrados * 9/5) + 32
print ("La temperatura en grados fahrenheit es: \n", fahrenheit)